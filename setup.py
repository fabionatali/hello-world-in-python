import setuptools

setuptools.setup(
    name="hello-world-in-python",
    version="0.0.0",
    author="",
    author_email="",
    description="Hello world in Python",
    url="https://gitlab.com/fabionatali/hello-world-in-python/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
